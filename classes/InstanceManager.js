require('dotenv').config()
const redis = require('../utils/redis');
const { Docker } = require('node-docker-api');
const docker = new Docker({ socketPath: '/var/run/docker.sock' });
const moment = require('moment');
const axios = require('axios');
const Pubsub = require('./PubSub');
const { execSync } = require('child_process');


/**
 * Instance manager
 * @class
 */
class InstanceManager {
  /** 
   * @param 
   * @param {!string} subscriptionName Subscription image name in google Pub/Sub
   * @param {!string} imageName The name of the base image
   * @param {!number} maxInstances The maximum number of instances to manage
   * @param {!number} [startingPort=3000] What port this process should start the process on
   * @param {!pubsubConfig}
  */
  constructor(subscriptionName, imageName, maxInstances, startingPort, pubsubConfig) {
    this.pubsub = new Pubsub(pubsubConfig, subscriptionName);
    this.maxInstances = maxInstances;
    this.imageName = imageName;
    this.startingPort = startingPort || 3000;
    this.latestTag = process.env.LATEST_TAG || 'latest';
    this.url = 'http://localhost';

    this.freeInstances = [];
    this.busyInstances = [];
    this.reservedInstances = [];
    this.pendingUpdates = [];

    this.init();
  }

  async healthCheck(port) {
    try {
      await axios.get(`${this.url}:${port}/`);
    } catch (e) {
      console.log(`port ${port} seems down, restarting`);
      this.runImage(port);
    }
  }

  async init() {
    try {
      await this.setUpdateStatus();

      await this.spawnInstances();
      setInterval(() => {
        this.spawnInstances()
      }, 3 * 1000);

      setInterval(() => {
        this.manage()
      }, 10 * 1000)
    } catch (e) {
      console.error(e);
    }
  }

  async spawnInstances() {
    try {
      for (let i = 0; i < this.maxInstances; i++) {
        const url = this.url + ":" + (this.startingPort + i)
        if (this.reservedInstances.indexOf(url) === -1) {
          console.log(`health check for port ${this.startingPort + i}`)
          this.healthCheck(this.startingPort + i)
          const resp = await this.getContainerStatus(url);
          if (resp.status === 'FREE') {
            let urlIndex = this.freeInstances.indexOf(url);
            if (urlIndex === -1) {
              this.freeInstances.push(url)
            } else {
              this.busyInstances.splice(urlIndex, 1)
            }
          }
          else {
            let urlIndex = this.busyInstances.indexOf(url);
            if (urlIndex === -1) {
              this.busyInstances.push(url)
            } else {
              this.freeInstances.splice(urlIndex, 1)
            }
          }
        } else {
          console.log(`skipping ${url} because it's marked as being updated`)
        }
      }
    } catch (e) {
      console.error('fail to spawn instance, trying to pull image');
      console.log('===')
      console.log(this.latestTag);
      console.log('===')
      await this.downloadImage(this.latestTag);
    }
    
  }

  assignFreeInstance() {
    const url = this.freeInstances.pop();
    this.busyInstances.push(url)
    return url
  }

  releaseInstance(url) {
    const index = this.busyInstances.indexOf(url)
    if (index > -1) {
      this.busyInstances.splice(index, 1)
      return url
    }
    return null
  }

  async manage() {
    try {
      const { errors, messages } = this.pubsub.getMessages();
      const pubsubData = this.sortMessages(messages);
      const message = pubsubData ? this.filterPubsub(pubsubData) : null;
      if (message) {
        this.latestTag = image.split(':')[1];
        await this.setUpdateStatus(this.latestTag, true);
        await this.downloadImage(message.data.tag);
        this.pendingUpdates = this.freeInstances.concat(this.busyInstances);
        await this.commenceUpdate(message.data.tag);
      }
    } catch (e) {
      console.error(e);
      throw e;
    }
  }

  setUpdateStatus(tag = 'latest', updating = false) {
    return redis.setData('imageDetails', JSON.stringify({
      latestTag: tag,
      isUpgrading: updating
    }))
  }

  async commenceUpdate() {
    const updater = setInterval(async () => {
      if (this.pendingUpdates.length > 0) {
        if (this.reservedInstances.length === 0) {
          await this.reserveInstances();
          await this.updateContainers();
        }
      } else {
        this.setUpdateStatus(this.latestTag, false);
        clearInterval(updater);
      }
    }, 5000)
  }
  async reserveInstances(){
    for (let i = 0; i < Math.ceil(this.maxInstances * 0.1); i++) {
      const url = this.pendingUpdates.pop();
      this.reservedInstances.push(url);
    }
  }

  /**
   * @param {pubSubMessage []} list - list of arrays from pubsub
   * @returns {pubSubMessage []} - filtered arrays based on current image name
   */
  filterPubsub(pubsubMessages) {
    for (let i = 0; i < pubsubMessages.length; i++) {
      if (pubsubMessages[i].data.action == 'INSERT' &&
         pubsubMessages[i].data.tag.includes(this.imageName)) {
        return pubsubMessages[i];
    }
  }
}

  promisifyStream (stream) {
    return new Promise((resolve, reject) => {
      stream.on('data', d => '');
      stream.on('end', resolve);
      stream.on('error', reject);
    })
  }

  /**
   * 
   * @param {string} port - the image tag to run
   */
  async runImage(port) {
    try {
      const instancePort = '3000/tcp';
      const containerOption = {
        Image: `${this.imageName}:${this.latestTag}`,
        Tag: this.latestTag,
        Labels: {
          imageVersion: this.latestTag
        },
        ExposedPorts: {
          [instancePort]: {}
        },
        HostConfig: {
          PortBindings: {
            [instancePort]: [{
              HostIp: '127.0.0.1',
              HostPort: String(port)
            }]
          }
        }
      }
      const container = await docker.container.create(containerOption);
      container.start();
    } catch (e) {
      console.error(e)
    }
  }

  /**
   * @returns array of containers
   * @description returns all of the running container in this machine
   */
  getContainers() {
    return docker.container.list();
  }

  /**
   * @function findAndRestart - Find container with specified port and restart
   * @param {number} portNumber - Container port number
   */
  async restartContainer(portNumber) {
    const container = await this.getContainerInstance(portNumber);
    return await container.restart();
  }

  async waitContainer(url) {
    const resp = await this.getContainerStatus(url)
    if (resp !== 'FREE') {
      return await this.waitContainer(url)
    } else {
      return resp
    }
  }

  async updateContainers() {
    this.reservedInstances.forEach(async url => {

      await this.waitContainer(url);

      console.log(`updating container with url ${url}`);
      const port = url.split(':')[2];
      const container = await this.getContainerInstance(port);
  
      console.log(`stopping container..`);
      await container.stop();
      console.log(`container with url ${url} stopped`)
      console.log(`deleting container..`)
      await container.delete({ force: true });
      console.log(`container deleted`)
      await this.runImage(port);
      console.log(`spawned new image in port ${port}`)
      const instanceIndex = this.reservedInstances.indexOf(url);
      this.reservedInstances.splice(instanceIndex, 1);
    })
  }

  async getContainerInstance(port) {
    const containerInstances = await this.getContainers();
    for (let i = 0; i < containerInstances.length; i++) {
      const instanceDetails = await containerInstances[i].status()
      const instancePort = instanceDetails.data.HostConfig.PortBindings['3000/tcp'][0].HostPort;
      if (instancePort === port) {
        return instanceDetails;
      }
    }
    return null;
  }

  async downloadImage(image) {
    const tag = image.split(':')[1] || image;
    console.log(`downloading image with tag ${tag}`);
    // executing shell command so that we can get authentication from the docker library
    const shellMsg = execSync(`docker pull ${this.imageName}:${ tag || this.latestTag}`);
    console.log(shellMsg.toString())
    return
  }

  sortMessages (pubSubMessages) {
    const timestamps = pubSubMessages.map(time => {
      return {
        unix: moment(time.publishTime).unix(),
        data: JSON.parse(Buffer.from(time.data, 'base64').toString())
      };
    })
    timestamps.sort((a, b) => b.unix - a.unix);
    return timestamps;
  }


  async getContainerStatus(url) {
    const resp = await axios.get(url);
    return resp.data.status;
  }
}

try {
  let CM = new InstanceManager('projects/taralite-experimental/subscriptions/instance-manager-test', 'asia.gcr.io/taralite-experimental/dummy-express', 2, 3000, {
    projectId: 'taralite-experimental',
    keyFilename: './ignore/keyfile.json'
  });
} catch (e) {
  console.error(e)
}

module.exports = InstanceManager