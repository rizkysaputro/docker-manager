require('dotenv').config();
const PubSub = require(`@google-cloud/pubsub`);

  /**
   * @typedef {Object} pubSubMessage
   * @property {string} action
   * @property {string} digest
   * @property {string} tag
   */

  /**
   * @typedef {Object} pubsubConfig
   * @property {!string} projectId project id in gcp
   * @property {!string} keyFilename path to keyfile
   */

   /**
    * @class
    */
class Pubsub {
  /**
   * @param {pubsubConfig=} pubsubConfig Pub/Sub config object
   * @param {!string} subscriptionName Pub/Sub subsrciption name
   */
  constructor(pubsubConfig = {}, subscriptionName) {
    const { projectId, keyFilename } = pubsubConfig;
    if (pubsubConfig) {
      this.pubsub = new PubSub(pubsubConfig);
    } else {
      this.pubsub = new PubSub();
    }
    this.subscriptionName = subscriptionName || process.env.SUBSCRIPTION_NAME;
    this.subscription = this.pubsub.subscription(this.subscriptionName);
    this.messages = [];
    this.errors = [];
    this.subscription.on(`message`, message => {
      this.messages.push(message);
      message.ack();
    });
    this.subscription.on(`error`, error => {
      this.errors.push(error);
    });
  }

  /** 
   * @function listen - Listen for pubsub message
   * @returns {Promise<pubSubMessage[]>} - Array of pubsub messages
   */
   getMessages() {
    return {
      errors : this.errors.splice(0),
      messages : this.messages.splice(0),
    };
  }
}

module.exports = Pubsub;
