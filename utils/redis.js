const promise = require('bluebird');
const redis = require('redis')
promise.promisifyAll(redis.RedisClient.prototype);
promise.promisifyAll(redis.Multi.prototype);

const client = redis.createClient({});

client.on("error", function (err) {
  console.log("Error " + err);
});


/**
 * @param {string} key - key name in redis
 * @returns {Promise<string>} - string 
*/
const getData = async key => {
  return await client.getAsync(key)
}

/**
 * @param {string} key - key name to save to redis
 * @param {any} value - the value to be saved
 * @param {number} timeout - (in second) if set, will set the key expiration date
*/ 
const setData = async (key, value, timeout) => {
  if (timeout) {
    return await client.setexAsync(key, timeout, value)
  } else {
    return await client.setAsync(key, value)
  }
}

module.exports = {
  getData,
  setData
}