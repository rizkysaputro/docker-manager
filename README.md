# Docker Manager
Maintains containers and managing them, such as updating to the latest version, killing unresponsive container after being busy for specified duration

## Jobs

### Check Statuses
This service will check each of the instances status every specified time duration and save the result to redis

### Check Image Version
This service will check for new image version periodically. if newer version is found, get ready for updating all containers 

### Update Containers
Check for every idle container every minutes, if idle container found, kill the idle container and run new version of the container

### File Example
This repo contains some file example of the data that is saved in the redis instance the service is running on