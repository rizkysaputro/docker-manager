#!/bin/sh
echo 'Preparing .env file'
echo $1 | base64 --decode > .env
echo 'Finished preparing .env file'